package com.littlebutenough.webviewmaker

import android.app.PendingIntent
import android.content.Context
import android.content.pm.ShortcutInfo
import android.content.pm.ShortcutManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.annotation.RequiresApi

import com.littlebutenough.webviewmaker.singleton.Shortcuts
import com.littlebutenough.webviewmaker.singleton.shortcut_website_id


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Test Diffence


        //View binding
        val createShotCutBT = findViewById<Button>(R.id.btn_youtube_sc)

        //Check's that the api version meets the requirements
        if (Build.VERSION.SDK_INT >= 25) {
            //Sets up our shortcuts
            Shortcuts.setUp(applicationContext)
        }

        //Check's that the api version meets the requirements
        if (Build.VERSION.SDK_INT >= 28) {

            //Takes care of activating the pinned shortcuts
            createShotCutBT.setOnClickListener {
                shortcutPin(applicationContext, shortcut_website_id, 0)
            }
        }



    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun shortcutPin(context: Context, shortcut_id: String, requestCode: Int) {

        val shortcutManager = getSystemService(ShortcutManager::class.java)

        if (shortcutManager!!.isRequestPinShortcutSupported) {
            val pinShortcutInfo =
                ShortcutInfo.Builder(context, shortcut_id).build()

            val pinnedShortcutCallbackIntent =
                shortcutManager.createShortcutResultIntent(pinShortcutInfo)

            val successCallback = PendingIntent.getBroadcast(
                context, /* request code */ requestCode,
                pinnedShortcutCallbackIntent, /* flags */ PendingIntent.FLAG_IMMUTABLE
            )

            shortcutManager.requestPinShortcut(
                pinShortcutInfo,
                successCallback.intentSender
            )
        }
    }


}