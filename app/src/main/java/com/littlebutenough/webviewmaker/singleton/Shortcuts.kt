package com.littlebutenough.webviewmaker.singleton

import android.content.Context
import android.content.Intent
import android.content.pm.ShortcutInfo
import android.content.pm.ShortcutManager
import android.graphics.drawable.Icon
import androidx.core.content.ContextCompat.getSystemService
import com.littlebutenough.webviewmaker.R
import com.littlebutenough.webviewmaker.WebViewActivity


const val shortcut_website_id = "id_website"
val shortcut_url = "https://www.youtube.com/codepalace"


object Shortcuts {
    fun setUp(context: Context) {
        val shortcutManager =
            getSystemService<ShortcutManager>(context, ShortcutManager::class.java)

        val intent =Intent(Intent.ACTION_VIEW, null, context, WebViewActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        val shortcut = ShortcutInfo.Builder(context, shortcut_website_id)
            .setShortLabel("Website")
            .setLongLabel("Open the website")
            .setIcon(Icon.createWithResource(context, R.drawable.ic_baseline_public))
            .setIntent(
                intent
            )
            .build()
        shortcutManager!!.dynamicShortcuts = listOf(shortcut)
    }
}